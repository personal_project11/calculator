
/*Define some variables to perform operations*/
let number1 = 0, number2 = 0,  operation = 0, total = 0;
/*----------------------------------------------*/
/*Getting buttons from html */
const $number_display = document.querySelector("#number_display"),
    $operations_display = document.querySelector("#operations_display"),
    $number_buttons = document.querySelectorAll(".button_number"),
    $operations_buttons = document.querySelectorAll(".operations"),
    $reset_button = document.querySelector(".reset");


/*Initializing display at 0 values*/
$number_display.value = "0";
$operations_display.value = "0";


/*This is the callback which will be called whenever 
the user presses any number*/ 
const number_event = (event)=>{

    console.log(event.target.textContent);

    if($number_display.value === "0")
    {
        $number_display.value = event.target.textContent;
    }else if($number_display.value.length < 10 || ($number_display.value[0] === "-" 
             && $number_display.value.length < 11)){
        $number_display.value += event.target.textContent;
    }
    else{
        alert("You cannot introduce any more number," 
        + "number limit is 10!!!")
    }
}

/*This is the callback which is gonna be called
when an operation is performed*/
const operations_event = (event) => {


    switch(event.target.textContent) {
        case '+':
            $operations_display.value = $number_display.value + " + ";
            number1 = parseInt($number_display.value);
            operation = 1;//Operation 1 means add
            $number_display.value = "0";
            break;
            
        case '-':
            $operations_display.value = $number_display.value + " -";
            $number_display.value = "0";
            break;

        case 'x':
            $operations_display.value = $number_display.value + " x"; 
            $number_display.value = "0";
            break;

        case '/':
            $operations_display.value = $number_display.value + " /"; 
            $number_display.value = "0";
            break;
        case '=':
            $operations_display.value += $number_display.value;
            number2 = parseInt($number_display.value);
            total = number1 + number2;
            $number_display.value = total.toString();
            break;
        }
}

/*This callback will be called when reset button is pressed */
const reset_event = (event) => {
    $number_display.value = "0";
    $operations_display.value = "0";
}

/*This gives an attachment to every number button with the number_event*/
$number_buttons.forEach((el, index) => {
    el.addEventListener("click", number_event);
});

/*Attachment to operations button with operations_event*/
$operations_buttons.forEach((el, index) => {
    el.addEventListener("click", operations_event);
});

/*This gives an attachment to reset button with the reset_event */
$reset_button.addEventListener("click", reset_event);